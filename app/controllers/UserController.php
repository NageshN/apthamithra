<?php
	class UserController extends BaseController{
		//normal signup 
		public function createUser($userDetail){
			//Call a model to create user
			//$result = User :: all();
			$result = User::createUser($userDetail);
			return $result;
		}
		
		//normal login 
		public function userLogin($data){
			$data = base64_decode($data);
			$data = json_decode($data,true);
		
			/*$result = User::userLogin($data); */	
				$isAuth = Auth::attempt($data);
			
			if($isAuth)
			{
				$userData = User :: where('email_id', '=' ,$data['email_id'])->get();
				$result = array('status' => 'success','response' => 'user login success',$userData);
				return $result;
			}
			else
			{
				$result = array('status' => 'failure','response' => 'user login failure');
				return $result;
			}
			
			
		}
		
		//login with facebook
		public function loginWithFacebook($userDataFromFacebook){
			$userDataFromFacebook = base64_decode($userDataFromFacebook);
			$userDataFromFacebook = json_decode($userDataFromFacebook,true);
			$result = User::createUserFromFacebook($userDataFromFacebook);
			
			if($result == 409)
			{
				return array('status' => 'failure','response' => 'Email Id already has been used');
				
			}
			else if($result)
			{
				// we are now logged in, go to home
				$userData = User :: where('email_id', '=' ,$userDataFromFacebook['email'])->first();
				$result = array('status' => 'success','response' => 'user login success',$userData);
				return $result;
				
			}
			else{
				return array('status' => 'failure','response' => 'Login with facebook failed');
			}
		}
		
		
		//login with google
		public function loginWithGoogle($userDataFromGoogle){
			$userDataFromGoogle = base64_decode($userDataFromGoogle);
			$userDataFromGoogle = json_decode($userDataFromGoogle,true);
			$result = User::createUserFromGoogle($userDataFromGoogle);
		
			if($result == 409)
			{
				return array('status' => 'failure','response' => 'Email Id already has been used');
				
			}
			else if($result)
			{
				// we are now logged in, go to home
				$userData = User :: where('email_id', '=' ,$userDataFromGoogle['email'])->first();
				$result = array('status' => 'success','response' => 'user login success',$userData);
				return $result;
				
			}
			else{
				return array('status' => 'failure','response' => 'Login with google failed');
			}
		}
		
		
		
	}

?>