<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	public $timestamps = false;
	protected $fillable = ['email_id','password'];
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	
	public static function createUser($userDetail){
		$userDetail = base64_decode($userDetail);
		$data = json_decode($userDetail,true);
		$users = User :: where('email_id','=',$data['email_id'])->get();
		$noOfUsers = count($users);
		
			if($noOfUsers > 0){
				$result = array('status' => "Failure","response" => "EmailId already exist");
			}
			else{
				$user = new User;
				$user->full_name = $data['full_name'];
				$user->email_id = $data['email_id'];
				$user->password = Hash::make($data['password']);
				$user->registered_through = $data['registered_through'];
				$user->phone_number = $data['phone_number'];
				$user->save();
				$user = User :: where('email_id','=',$data['email_id'])->first();
				$result = array('status' => "Success","response" => "Success",$user);
			}
		return ($result);
	}
	
	
	public static function createUserFromFacebook($data)
    {
		$socialUserId = $data['id'];
		
		$user = User::where('social_user_id','=',$socialUserId)->first();
		
		
		if(empty($user)){
			$users = User :: where('email_id','=',$data['email'])->get();
			$noOfUsers = count($users);
			
			if($noOfUsers > 0){
				//email Id already exist
				return 409;
			}
			$user = new User;
			$user->full_name = $data['name'];
			$user->email_id = $data['email'];
			//$user->password = Hash::make($data['password']);/* 
			$user->registered_through = "Facebook";
			$user->gender = $data['gender'];
			$user->social_user_id = $socialUserId;
			$profilePicture = 'https://graph.facebook.com/'.$socialUserId.'/picture?type=large';
			$user->	profile_picture = $profilePicture; 
			$user->save(); 
			return 1;
		}
		return 1;
		
    }
	public static function createUserFromGoogle($data)
    {
		$socialUserId = $data['id'];
		
		$user = User::where('social_user_id','=',$socialUserId)->first();
		
		if(empty($user)){
			$users = User :: where('email_id','=',$data['email'])->get();
			$noOfUsers = count($users);
			
			if($noOfUsers > 0){
				//email Id already exist
				return 409;
			}
			$user = new User;
		
			$user->full_name = $data['name'];
			$user->email_id = $data['email'];
 			$user->gender = $data['gender'];
			
			$user->registered_through = "Google";
			$user->social_user_id = $socialUserId;
			 $user->profile_picture = $data['picture'];
			$user->save(); 
			return 1;
		}
			return 1;
    }
	
	

}
