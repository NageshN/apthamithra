<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::post('createUser/{userDetail}', 'UserController@createUser');
Route::get('loginWithFacebook/{userDataFromFacebook}',array('as'=>'loginWithFacebook','uses'=>'UserController@loginWithFacebook'));
Route::get('loginWithGoogle/{userDataFromGoogle}',array('as'=>'loginWithGoogle','uses'=>'UserController@loginWithGoogle'));
Route::get('userLogin/{userLoginInfo}', 'UserController@userLogin');
Route::post('user/complaint/upload/image', 'UserComplaintController@upload');
Route::post('user/complaint/upload/{complaintDetails}', 'UserComplaintController@uploadComplaint');

Route::get('showUser', function()
	{
	$users = User::all();
	return $users;
	});
	
	Route::get('upload', function(){

	return View::make('image');
});


